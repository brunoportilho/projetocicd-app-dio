#!/bin/bash

## Image Names
## APP
# bportilho/projetocicd-app:1.0

echo "Build de Imagens"
docker build -t bportilho/projetocicd-app:1.0 app/.

echo "Push de Imagens"
docker push bportilho/projetocicd-app:1.0

echo "Deploy"
kubectl apply -f ./database-v1.0.yml --record
kubectl apply -f ./app-deploy-v1.0.yml --record